// utilizzo paramentri youtube
// guida: https://developers.google.com/youtube/player_parameters
// ----------------------------------------------------------------
// url src da utilizzare per l'iframe:
// https://www.youtube.com/embed/VIDEO_ID?enablejsapi=1&controls=0&fs=0&iv_load_policy=3&rel=0&showinfo=0&loop=1&modestbranding=1&playlist=VIDEO_ID
// sostituire VIDEO_ID con l'id youtube del video
// ----------------------------------------------------------------
// parametro enablejsapi=1 tenerlo sempre presente nell'url dell'iframe per poter utilizzare le youtube api con controllo eventi su embed


///VARIABILI DA VALORIZZARE ATTRAVERSO IL WMC RELATIVE ALLO SLIDER DI HOME
//
// var caruosel_speed = 2000 /// in millisec
// var caruosel_transition_speed = 500 /// in millisec
// var caruosel_autoplay = true /// determina il comportamento dello slider al loading della pagina
// var caruosel_pause_on_hover = true /// determina il comportamento dello slider in hover attivo/disattivo


$(document).ready(function($) {
    /* class per mobile */
    var isMobile = false;
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
        isMobile = true;
        $("body").addClass("mobile");
    }

    var OSName="os-unknown";
    if (navigator.appVersion.indexOf("Win")!=-1) OSName="os-windows";
    else if (navigator.appVersion.indexOf("Mac")!=-1) OSName="os-macOS";
    else if (navigator.appVersion.indexOf("X11")!=-1) OSName="os-unix";
    else if (navigator.appVersion.indexOf("Linux")!=-1) OSName="os-linux";
    $('html').addClass(OSName);


    /* rimuove status :hover per mobile - problema double tab per link su iOS per le card a sfondo blu in hover */
    function hasTouch() {
        return 'ontouchstart' in document.documentElement
               || navigator.maxTouchPoints > 0
               || navigator.msMaxTouchPoints > 0;
    }
    if (hasTouch()) { // remove all :hover stylesheets
        try { // prevent exception on browsers not supporting DOM styleSheets properly
            for (var si in document.styleSheets) {
                var styleSheet = document.styleSheets[si];
                if (!styleSheet.rules) continue;

                for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
                    if (!styleSheet.rules[ri].selectorText) continue;

                    if (styleSheet.rules[ri].selectorText.match("a.contentbox:hover")) {
                        // console.log(styleSheet.rules[ri].selectorText);
                        styleSheet.deleteRule(ri);
                    }
                }
            }
        } catch (ex) {}
    }


    /* change tab on hover megamenu */
    $('#mainmenu .nav-item .nav-link').on('mouseover', function() {
        var activeLink = $(this);
        var tab = activeLink.attr('aria-controls');

        // cambio hover voce sezione
        $("#mainmenu .nav-link").each(function() {
            $(this).removeClass("active");
        });
        activeLink.addClass("active");

        // cambio tab in hover
        $("#mainmenuContent .tab-pane").each(function() {
            $(this).removeClass("active show");
        });
        $("#mainmenuContent #"+tab).addClass("active show");
    });

    /* link on click tab megamenu */
    $('#mainmenu .nav-item .nav-link').on('click', function() {
        window.location.href = $(this).attr("href");
    });

    /* close megamenu on click a */
    $('#mainmenuContent.tab-content a').on('click', function() {
        $("header.head").removeClass("active");
        $("#navbarMenu").removeClass("show");
        $(".search-container").slideUp(250);
    });
    $('#navbarMenuMobile .dropdown-menu a').on('click', function() {
        if ($(this).hasClass("dropdown-toggle") === false) {
            $("header.head").removeClass("active");
            $("#navbarMenuMobile").removeClass("show");
            $("html, body").toggleClass("fixedPosition");
            $(".search-container").slideUp(250);
        }
    });

    function scrollToAnchor(hash, fix) {
        var target = $(hash);
        if ($(this).width() < 992) {
            var headerHeight = $("header.head").height() + 10; // Get fixed header height
        } else {
            var headerHeight = $("header.head").height() + 20;
        }

        target = target.length ? target : $('[name=' + hash.slice(1) +']');

        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - headerHeight
            }, 500);
            return false;
        }
    }
    if (window.location.hash) {
        scrollToAnchor(window.location.hash);
    }
    $("a[href*=\\#]:not([href=\\#])").on("click", function() {
        if ($(this).data("toggle") === undefined) {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
                || location.hostname == this.hostname) {
                scrollToAnchor(this.hash);
            }
        }
    });


    /* sticky menu per mobile */
    var $el = $('header.head');
    $(window).bind("scroll", function (e) {

        if ($(window).width() < 992) {
            if ($(window).scrollTop() > 80) {
                $el.addClass("sticky");
            } else {
                $el.removeClass("sticky");
            }
        } else {
            if ($(window).scrollTop() > 80){
                $el.addClass("sticky mini");
            }
            if ($(window).scrollTop() < 80){
                $el.removeClass("sticky mini");
            }
        }
    });
    $(window).on("resize", function(){

        if ($(window).width() < 992) {
            $el.removeClass("mini");
        } else {
            $el.addClass("mini");
        }
        // if ($(this).width() >= 992) {
        //     $("header.head").removeClass("sticky");
        // }
    });


    /* home slider */
    var iframes = $('.home-slider').find('.embed-player');
    $('.home-slider').on('init', function(event, slick){
        // if (!($('.home-slider .slick-slide').length > 1)) {
        //     $('.home-slider .slick-dots').hide();
        // }
        // if ($('.home-slider .slick-slide').length <= 1) {
        //     $('.home-slider .slick-dots').hide();
        // }
        // if (window.outerWidth <= 880) {
        //     if (!($('.home-slider .slick-slide').length > 1)) {
        //         $('.home-slider .slick-dots').each(function() {
        //             $(this).hide();
        //         })
        //     }
        // }
        slick = $(slick.currentTarget);
        if (isMobile === false) {
            setTimeout(function(){
            playPauseVideo(slick,"pause");
            }, 1000);
        } else {
            playPauseVideo(slick,"pause");
        }
        resizePlayer(iframes, 16/9);

        // controllo frecce per slide senza caption
        $('.home-slider .slick-slide').each(function() {
            if ($(this).find('.caption').length <= 0) {
                $(this).find('.caption-container').addClass('nocaption');
            } 
        });
    });
    // $(window).on("resize", function(){
    //     // if (!($('.home-slider .slick-slide').length > 1)) {
    //     //     $('.home-slider .slick-dots').hide();
    //     // }
    //     // if ($('.home-slider .slick-slide').length <= 1) {
    //     //     $('.home-slider .slick-dots').hide();
    //     // }
    //     // if (window.outerWidth <= 880) {
    //     //     // if (!($('.home-slider .slick-slide').length > 1)) {
    //     //     //     $('.home-slider .slick-dots').each(function() {
    //     //     //         $(this).hide();
    //     //     //     })
    //     //     // }
    //     // }
    // });
    $('.home-slider').on("beforeChange", function(event, slick) {
        slick = $(slick.$slider);
        if (isMobile === false) {
            playPauseVideo(slick,"pause");
        } else {
            playPauseVideo(slick,"pause");
        }
    });
    $('.home-slider').on("afterChange", function(event, slick) {
        slick = $(slick.$slider);
        if (isMobile === false) {
            playPauseVideo(slick,"pause");
        } else {
            playPauseVideo(slick,"pause");
        }
    });
    $('.home-slider').slick({
        //autoplay: caruosel_autoplay,
        dots: false,
        infinite: true,
        //autoplaySpeed: caruosel_speed,
        //pauseOnHover:caruosel_pause_on_hover,
        //speed: caruosel_transition_speed,
        fade: true,
        cssEase: 'linear',
        adaptiveHeight: true,
        //lazyLoad: 'ondemand', controls: false,
        prevArrow: $(".slickhome-prev"),
        nextArrow: $(".slickhome-next"),
        rows:0,
        responsive: [
            {
                breakpoint: 881,
                settings: {
                    dots: true,
                    adaptiveHeight: true,
                    arrows: false,
                    rows:0

                }
            }
        ]
    });
    // $(window).load(function() {
    //     if (window.outerWidth <= 880) {
    //         if (!($('.home-slider .slick-slide').length > 1)) {
    //             $('.home-slider .slick-dots').each(function() {
    //                 $(this).hide();
    //             })
    //         }
    //     }
    // });
    // $('.home-slider .slick-slide').each(function() {
    //     if ( !$(this).find(".caption-container .caption").length ) {
    //         $(this).find(".caption-container").addClass("nocontent");
    //     }
    // });
    // settings for homepage slider autoplay
    // $("main:not(.magazinenews) .home-slider").slick('slickSetOption', {
    //     'autoplay': true, 'infinite': true, 'autoplaySpeed': 5000
    // }, true);
    // $("main:not(.magazinenews) .home-slider").slick('refresh');
    // $("main:not(.magazinenews) .home-slider").slick('slickSetOption', {
    //     'autoplaySpeed': 5000
    // }, 5000);
    // $("main:not(.magazinenews) .home-slider").slick('slickSetOption', {
    //     'infinite': true
    // }, true);
    $("main:not(.magazinenews) .home-slider .slickhome").removeClass('slick-disabled');
    $(".slickhome.slickhome-prev, .slickhome.slickhome-next").on('click', function() {
        $('.home-slider').slick('slickPause');
    });
    // check video player on click
    var playerDivs = document.querySelectorAll(".home-slider .embed-player.slide-media");
    var playerDivsArr = [].slice.call(playerDivs);
    var players = new Array(playerDivsArr.length);
    var waypoints = new Array(playerDivsArr.length);
    // add custom id to embed iframe youtube
    playerDivsArr.forEach(function(e, i) {
        e.setAttribute("id", "iframeyt-"+i);
    });
    // create youtube players
    // playerDivsArr.forEach(function(e, i) {
    //     players[i] = new YT.Player(e.id, {
    //         playerVars: {"origin": window.location.href},
    //         events: {
    //             //'onReady': onPlayerReady,
    //             'onStateChange': onPlayerStateChange
    //         }
    //     })
    // });
    function onYouTubeIframeAPIReady() {
        // console.log("hey Im ready");
        playerDivsArr.forEach(function(e, i) {
            players[i] = new YT.Player(e.id, {
                playerVars: {"origin": window.location.href},
                events: {
                    //'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            })
        });
    }
    // function onPlayerReady() {
    //     console.log("hey Im ready");
    // }
    function onPlayerStateChange(event) {
        // data = -1 – unstarted, 0 – ended, 1 – playing, 2 – paused, 3 – buffering, 5 – video cued
        // console.log(event.data);
        if (event.data >= 0) {
            $('.home-slider').slick('slickPause');
        }
    }

    // POST commands to YouTube or Vimeo API
    function postMessageToPlayer(player, command){
        if (player == null || command == null) return;
        player.contentWindow.postMessage(JSON.stringify(command), "*");
    }
    // When the slide is changing
    function playPauseVideo(slick, control){
        var currentSlide, slideType, startTime, player, video;

        currentSlide = slick.find(".slick-current");
        //slideType = currentSlide.attr("class").split(" ")[1];
        player = currentSlide.find("iframe").get(0);
        startTime = currentSlide.data("video-start");

        // if (slideType === "vimeo") {
        //     switch (control) {
        //         case "play":
        //         if ((startTime != null && startTime > 0 ) && !currentSlide.hasClass('started')) {
        //             currentSlide.addClass('started');
        //             postMessageToPlayer(player, {
        //             "method": "setCurrentTime",
        //             "value" : startTime
        //             });
        //         }
        //         postMessageToPlayer(player, {
        //             "method": "play",
        //             "value" : 1
        //         });
        //         break;
        //         case "pause":
        //         postMessageToPlayer(player, {
        //             "method": "pause",
        //             "value": 1
        //         });
        //         break;
        //     }
        // } else if (slideType === "youtube") {
            switch (control) {
                case "play":
                postMessageToPlayer(player, {
                    "event": "command",
                    "func": "mute"
                });
                postMessageToPlayer(player, {
                    "event": "command",
                    "func": "playVideo"
                });
                break;
                case "pause":
                postMessageToPlayer(player, {
                    "event": "command",
                    "func": "pauseVideo"
                });
                break;
            }
        // } else if (slideType === "video") {
        //     video = currentSlide.children("video").get(0);
        //     if (video != null) {
        //         if (control === "play"){
        //         video.play();
        //         } else {
        //         video.pause();
        //         }
        //     }
        // }
    }
    // Resize player
    function resizePlayer(iframes, ratio) {
        if (!iframes[0]) return;
        var win = $(".home-slider"),
            width = win.width(),
            playerWidth,
            height = win.height(),
            playerHeight,
            ratio = ratio || 16/9;

        iframes.each(function(){
        var current = $(this);
        if (width / ratio < height) {
            playerWidth = Math.ceil(height * ratio);
            current.width(playerWidth).height(height).css({
            left: (width - playerWidth) / 2,
            top: 0
            });
        } else {
            playerHeight = Math.ceil(width / ratio);
            current.width(width).height(playerHeight).css({
            left: 0,
            top: (height - playerHeight) / 2
            });
        }
        });
    }
    // Resize event
    $(window).on("resize.slickVideoPlayer", function(){
        resizePlayer(iframes, 16/9);
    });


    /* news slider */
    // $('.news-slider').on('init', function(event, slick){
    //     if (!($('.news-slider .slick-slide').length > 1)) {
    //         $('.news-slider .slick-dots').hide();
    //     }
    // });
    /*$('.news-slider').slick({
        autoplay: false,
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        cssEase: 'linear',
        lazyLoad: 'ondemand', controls: false,
    });*/
    $('.news-slider').slick({
        autoplay: false,
        dots: false,
        arrows: true,
        infinite: false,
        speed: 300,
        cssEase: 'linear',
        prevArrow: "<button type='button' class='slick-prev internal'><i class='far fa-arrow-left' aria-hidden='true'></i></button>",
        nextArrow: "<button type='button' class='slick-next internal'><i class='far fa-arrow-right' aria-hidden='true'></i></button>",
        lazyLoad: 'ondemand', controls: false,
    });


    /* slider widget */
    // $('.slider-box').on('init', function(event, slick){
    //     if (!($('.slider-box .slick-slide').length > 1)) {
    //         $('.slider-box .slick-dots').hide();
    //     }
    // });
    $('.slider-box').slick({
        autoplay: false,
        dots: true,
        arrows: true,
        infinite: true,
        speed: 300,
        cssEase: 'linear',
        lazyLoad: 'ondemand', controls: false,
        prevArrow: "",
        nextArrow: "<button type='button' class='slick-next small external'><i class='far fa-arrow-right' aria-hidden='true'></i></button>",
    });


    /* slider suggeriti per te */
    $('.slider-suggeriti').slick({
        autoplay: false,
        dots: false,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 2,
        cssEase: 'linear',
        lazyLoad: 'ondemand', controls: false,
        prevArrow: "<button type='button' class='slick-prev internal'><i class='far fa-arrow-left' aria-hidden='true'></i></button>",
        nextArrow: "<button type='button' class='slick-next internal'><i class='far fa-arrow-right' aria-hidden='true'></i></button>",
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
        ]
    });


    /* slider rettori */
    $('.slider-rettori').slick({
        autoplay: false,
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        cssEase: 'linear',
        lazyLoad: 'ondemand', controls: false,
        // prevArrow: "<button type='button' class='slick-prev small internal'><i class='far fa-arrow-left' aria-hidden='true'></i></button>",
        prevArrow: "",
        nextArrow: "<button type='button' class='slick-next small internal'><i class='far fa-arrow-right' aria-hidden='true'></i></button>",
        responsive: [
            {
                breakpoint: 880,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 620,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    });
    $(window).on("resize", function(){
        if ($('.slider-rettori').length > 0) {
            $(".rettori .info-rettore").matchHeight();
            $(".slider-rettori .slick-slide > div").matchHeight();
        }
    });


    /* slider ex studenti */
    $('.slider-exstudenti').slick({
        autoplay: false,
        dots: false,
        arrows: true,
        infinite: false,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        cssEase: 'linear',
        lazyLoad: 'ondemand', controls: false,
        // prevArrow: "<button type='button' class='slick-prev small internal'><i class='far fa-arrow-left' aria-hidden='true'></i></button>",
        prevArrow: "",
        nextArrow: "<button type='button' class='slick-next small internal'><i class='far fa-arrow-right' aria-hidden='true'></i></button>",
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 921,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });


    /* slider single generica */
    // $('.slider-singlegenerica').on('init', function(event, slick){
    //     if ($('.slider-singlegenerica .slick-slide').length <= 2) {
    //         $('.slider-singlegenerica .slick-dots').hide();
    //         // $('.slider-singlegenerica .slick-track').addClass('nobanda');
    //     }
    //     if ($('.slider-singlegenerica .slick-slide').length == 2) {
    //         $('.slider-singlegenerica').find('.slick-slide:eq(1)').addClass('novelina');
    //     }
    // });
    var $carousel_generica = $('.slider-singlegenerica');
    var settings = {
        // slide: '.slick-slideshow__slide',
        autoplay: false,
        dots: true,
        arrows: true,
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 1,
        variableWidth: true,
        adaptiveHeight: true,
        // prevArrow: "<button type='button' class='slick-prev small internal'><i class='far fa-arrow-left' aria-hidden='true'></i></button>",
        prevArrow: "<button type='button' class='slick-prev internal'><i class='far fa-arrow-left' aria-hidden='true'></i></button>",
        nextArrow: "<button type='button' class='slick-next internal right'><i class='far fa-arrow-right' aria-hidden='true'></i></button>",
    };
    $carousel_generica.slick(settings);
    setMaxWidth();
    $( window ).bind( "resize", setMaxWidth );
    function setMaxWidth() {
        var $divcontent = $(".content-single .container").first();
        // if ($(".contenuto-singola").length != 0) {
        //     var marginleftslider = $(".contenuto-singola").offset().left;
        //     // var marginrightslide = $carousel_generica.find('.slick-slideshow__slide.slick-current').offset().right;
        //     $( ".slider-singlegenerica" ).css( "maxWidth", ( $( window ).width() - marginleftslider ) + "px" );
        //     // $( ".slider-singlegenerica .slick-next" ).css( "right", marginrightslide + "px" );
        // }
        if ($divcontent.length != 0) {
            var marginleftslider = $divcontent.offset().left;
            // var marginrightslide = $carousel_generica.find('.slick-slideshow__slide.slick-current').offset().right;
            $( ".slider-singlegenerica" ).css( "maxWidth", ( $( window ).width() - marginleftslider ) + "px" );
            // $( ".slider-singlegenerica .slick-next" ).css( "right", marginrightslide + "px" );
        }
    }
    // se singola slide centro in slider-singlegenerica
    if ($('.slider-singlegenerica .slick-slide').length <= 1) {
        $('.slider-singlegenerica').addClass('single-slide');
        $('.slider-singlegenerica').slick('slickSetOption', {
            centerMode: true,
            responsive: [
                {
                    breakpoint: 880,
                    settings: {
                        slidesToShow: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: false,
                    }
                },
            ]
        }, true);
    } else {
        $('.slider-singlegenerica').slick('slickSetOption', {
            centerMode: false,
            responsive: [
                {
                    breakpoint: 880,
                    settings: {
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        variableWidth: false,
                    }
                },
            ]
        }, true);
    }


    /* standard slider */
    $('.standard-slider').on('init', function(event, slick){
        if (!($('.standard-slider .slick-slide').length > 3)) {
            $('.standard-slider .slick-dots').hide();
        }
    });
    $(".standard-slider").slick({
        autoplay: false,
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        cssEase: 'linear',
        lazyLoad: 'ondemand', controls: false,
        // prevArrow: "<button type='button' class='slick-prev'><i class='far fa-arrow-left' aria-hidden='true'></i></button>",
        // nextArrow: "<button type='button' class='slick-next'><i class='far fa-arrow-right' aria-hidden='true'></i></button>",
        responsive: [
            {
                breakpoint: 880,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                }
            },
        ]
    });

    /* videogallery slider */
    var numeroslide = 0;
    var setResponsive = 2;
    var argVideoSlider = {
        autoplay: false,
        dots: false,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToScroll: 1,
        cssEase: 'linear',
        lazyLoad: 'ondemand', controls: false,
        responsive: [
            {
                breakpoint: 880,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1
                }
            },
        ]
    };
    $('.videogallery-slider').on('init', function(event, slick){
        $(".btn-videogallery .slickvideo-prev").addClass("slick-disabled");

        // check items per disabilitare next slider
        var numeroslider = 0;
        var item_length = slick.slideCount;
        var sliderId = slick.$slider.context.id;
        if (slick.$slider.hasClass("video4")) {
            numeroslide = 4;
        } else if (slick.$slider.hasClass("video3")) {
            numeroslide = 3;
        } else if (slick.$slider.hasClass("video2")) {
            numeroslide = 2;
        }
        if (item_length <= numeroslide) {
            $("#"+sliderId).closest(".videogallery").find(".slickvideo-next").addClass("slick-disabled");
        }
    });
    $(".videogallery-slider").each(function(key, item) {
        // aggiungo id dinamico alla slider
        var sliderIdName = 'videogallery-slider' + key;
        this.id = sliderIdName;

        // check classe per video visibili
        if ($(this).hasClass("video4")) {
            numeroslide = 4;
        } else if ($(this).hasClass("video3")) {
            numeroslide = 3;
        } else if ($(this).hasClass("video2")) {
            numeroslide = 2;
        }
        argVideoSlider["slidesToShow"] = numeroslide;
        var sliderId = '#' + sliderIdName;
        $(sliderId).slick(argVideoSlider);

        // responsive
        if ($(this).hasClass("video4")) {
            $(sliderId).slick('slickSetOption', {
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                ]
            }, true);
        }

        // on change check frecce per disabled
        $(sliderId).on('afterChange', function(event, slick, currentSlide, nextSlide) {
            var item_length = slick.slideCount - 1;
            var nslide = 0;
            if ($(sliderId).hasClass("video4")) {
                if ($(window).width() < 1200 && $(window).width() >= 880) {
                    nslide = 3;
                } else if ($(window).width() < 880 && $(window).width() >= 660) {
                    nslide = 2;
                } else if ($(window).width() < 600) {
                    nslide = 1;
                } else {
                    nslide = 4;
                }
            } else if ($(sliderId).hasClass("video3")) {
                if ($(window).width() < 880 && $(window).width() >= 660) {
                    nslide = 2;
                } else if ($(window).width() < 600) {
                    nslide = 1;
                } else {
                    nslide = 3;
                }
            } else if ($(sliderId).hasClass("video2")) {
                if ($(window).width() < 600) {
                    nslide = 1;
                } else {
                    nslide = 2;
                }
            }
            // console.log(currentSlide+"--"+nslide+"--"+$(window).width());
            if( item_length == (currentSlide + (nslide - 1)) ){
                $(sliderId).closest(".videogallery").find(".slickvideo-next").addClass("slick-disabled");
            } else {
                $(sliderId).closest(".videogallery").find(".slickvideo-next").removeClass("slick-disabled");
            }
            if( currentSlide == 0 ){
                $(sliderId).closest(".videogallery").find(".slickvideo-prev").addClass("slick-disabled");
            } else {
                $(sliderId).closest(".videogallery").find(".slickvideo-prev").removeClass("slick-disabled");
            }
        });
    });
    $(".slickvideo-next").click(function(){
        $(this).closest(".videogallery").find(".videogallery-slider").slick('slickNext');
    });
    $(".slickvideo-prev").click(function(){
        $(this).closest(".videogallery").find(".videogallery-slider").slick('slickPrev');
    });


    /* menu toggler */
    $(".nav-desktop .navbar-toggler").on("click", function() {
        // reset tab selezionata in hover
        $("#mainmenu .nav-link").each(function() {
            $(this).removeClass("active");
        });
        $("#mainmenuContent .tab-pane").each(function() {
            $(this).removeClass("active show");
        });
        $("#mainmenu .nav-item:first-child .nav-link").addClass("active");
        $("#mainmenuContent .tab-pane:first-child").addClass("active show");
        // $(".link-visibili .search-menu").hide();

        // $('header.head').toggleClass('active');
        if ($(this).hasClass("collapsed")){
            $("header.head").addClass("active");
        } else {
            setTimeout(function() {
                $("header.head").removeClass("active");
                // $(".link-visibili .search-menu").show();
            }, 180);
            $(".search-container").slideUp(250);
            $("a.search").removeClass("active");
            $(".search-container").slideUp(250);
        }
    });
    $(".nav-mobile .navbar-toggler").on("click", function() {
        if ($(this).hasClass('collapsed')){
            $("header.head").addClass("active");
        } else {
            $("header.head").removeClass("active");
            $(".search-container").slideUp(250);
        }
        // tolgo classi da ricerca form
        $(".nav-mobile .search-mobile").removeClass('active');
        $(".nav-mobile .search-container").removeClass('active');
        $(".nav-mobile .search-input").blur();
        $("#navbarMenuMobile .show").each(function() {
            $(this).removeClass("show");
        });
        $("#navbarMenuMobile .open").each(function() {
            $(this).removeClass("open");
        });
        $("#navbarMenuMobile .aperto").each(function() {
            $(this).removeClass("aperto");
        });
        $(".search-container").slideUp(250);
        // fix position quando mobile menu è aperto
        $("html, body").toggleClass("fixedPosition");
    });


    /* height menu per mobile */
    var heightTop = $(".nav-mobile .navbar-brand").outerHeight() + 40; // 40 = 20+20 padding top-bottom
    $("#navbarMenuMobile").css({
        'height': $(window).outerHeight() - heightTop
    });
    // $("#navbarMenuMobile").css('height','100vh');
    $("#navbarMenuMobile").on("shown.bs.collapse hidden.bs.collapse", function() {
        var heightTop = $(".nav-mobile .navbar-brand").outerHeight() + 40; // 40 = 20+20 padding top-bottom
        $("#navbarMenuMobile").css({
            'height': $(window).outerHeight() - heightTop
        });
        // $("#navbarMenuMobile").css('height','100vh');
    });
    $(window).on("resize", function(){
        var heightTop = $(".nav-mobile .navbar-brand").outerHeight() + 40; // 40 = 20+20 padding top-bottom
        $("#navbarMenuMobile").css({
            'height': $(window).outerHeight() - heightTop
        });
        // $("#navbarMenuMobile").css('height','100vh');
    });
    // $(window).bind("scroll", function () {
    //     // var heightTop = $(".nav-mobile .navbar-brand").outerHeight() + 40; // 40 = 20+20 padding top-bottom
    //     // $("#navbarMenuMobile").css({
    //     //     'height': $(window).outerHeight() - heightTop
    //     // });
    //
    //     // $("header.head").bind('scroll', function () {
    //     //     $("#navbarMenuMobile").css('height','100vh');
    //     // });
    // });


    /* active su link menu */
    $(".main-menu a").on("click", function() {
        $(".main-menu a").removeClass("active");
        $(this).toggleClass("active");
    });


    /* form ricerca hide/show */
    $("a.search").on("click", function(e) {
        e.preventDefault();
        $(this).toggleClass("active");
        // $(".search-container").toggleClass('active');
        $(".search-container").slideToggle(250);
        $(".search-input").select().focus();
    });
    $("a.search-mobile").on("click", function(e) {
        e.preventDefault();
        $(this).toggleClass("active");
        // $(".nav-mobile .search-container").toggleClass('active');
        if ($(this).hasClass("active")) {
            $(".nav-mobile .search-container").slideDown(250);
            $(".nav-mobile .search-input").select().focus();
        } else {
            $(".nav-mobile .search-input").blur();
            $(".nav-mobile .search-container").slideUp(250);
        }
    });


    /* stessa altezza per div */
    $(".height-fullrow").matchHeight();
    $(".boxslider h2").matchHeight();
    $(".standard-slider h2").matchHeight();
    $(".offerta-iulm .offerta").matchHeight();
    $(".boxcorrelati .contentbox").matchHeight();
    $(".images-inside-post img").matchHeight();
    $(".boxslider .contentbox").matchHeight();
    $(".content-widget .box-single").matchHeight();
    $('.content-widget.box-ricerca-widget .box-single').matchHeight({ remove: true });
    $(".content-title-boxsnodo").matchHeight();
    $(".banda-info-dati .padbox").matchHeight();
    $(".banda-info-dati .padbox div").matchHeight();
    $(".exstudentibox").matchHeight();
    $(".rettori .info-rettore").matchHeight();
    $(".slider-singlegenerica .slick-slide p").matchHeight();
    $(".extra .list-single").matchHeight();
    $(".extra .extra-top").matchHeight();
    $(".box-new-contatti ul.nav li a").matchHeight();
    $(".primopiano .boxslider h2").matchHeight({ remove: true });
    $(".slider-rettori .slick-slide > div").matchHeight();

    /* fix altezza div slider video magazine */
    if ($('.sezione-magazine .primopiano').length > 0) {
        var heightFix = $(".sezione-magazine .primopiano .boxslider").height();
        $.fn.matchHeight._afterUpdate = function(event, groups) {
            // do something after all updates are applied
            $(".sezione-magazine .primopiano .boxslider a").css('height', heightFix);
        }
    }

    /* video bg header */
    if ($('.video-bg').length > 0) {
        $('.video-bg').bgVideo({
            fadeIn: 0,
            showPausePlay: false,
            pauseAfter: 0
        });
    }


    /* video popup */
    if ($('.poster-video a').length > 0 || $('.boxslider.video').length > 0) {
        $('.poster-video a, .boxslider.video a').flashy({
            // image, inline, ajax, iframe, video
            type: 'video',
            // show title
            title: true,
            // can be any CSS valid unit - px, %, and etc
            width: null,
            height: null,
            // enable/disable gallery mode
            gallery: false,
            // enable/disable infinite loop
            galleryLoop: true,
            // show item counter
            galleryCounter: true,
            // show prev and next navigation controls
            galleryPrevNext: true,
            // message used in the item counter. If empty, no counter will be displayed
            galleryCounterMessage: '[index] / [total]',
            // enable/disable swipe on desktop
            gallerySwipeDesktop: true,
            // enable/disable swipe on mobile devices
            gallerySwipeMobile: true,
            // set swipe threshold in px
            gallerySwipeThreshold: 100,
            // enable/disable keyboard navigation with arrows and close with ESC
            keyboard: true,
            // close lightbox via the close button or overlay click
            overlayClose: false,
            // additional css classes for customization
            themeClass: null,
            // enable/Disable video autoplay
            videoAutoPlay: false,
        });
    }


    /* dropdown multilevel menu mobile */
    $(".dropdown-menu a.dropdown-toggle").on("click", function(e) {
        /* modifica per nuova navigazione mobile
        - non utilizzato -

        // check per freccia aperta/chiusa link mobile
        if ($(this).hasClass("open")) {
            $(this).removeClass("open");
        } else {
            $("a.dropdown-toggle").each(function() {
                $(this).removeClass("open");
            });
            $(this).addClass("open");
        }
        //  check per last-child: aggiunge classe per border-bottom
        if ($(this).parent().is(":last-child")) {
            $("li.dropdown").each(function() {
                $(this).removeClass("last");
            });
            $(this).parent().addClass("last");
        } else {
            $("li.dropdown").each(function() {
                $(this).removeClass("last");
            });
        }
        if (!$(this).next().hasClass("show")) {
            $(this).parents(".dropdown-menu").first().find(".show").removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        var $toggler = $(this);
        $subMenu.toggleClass("show");
        $(this).parents("li.dropdown.show").on("hidden.bs.dropdown", function(e) {
            $(".dropdown-menu .show").removeClass("show");
            $toggler.removeClass("open");
        });
        */
        return false;
    });
    /* dropdown multilevel menu mobile - aggiunta toggler su voci di primo livello */
    $(".dropdown-linkcont a.dropdown-toggle, .dropdown-linkcont-second a.dropdown-toggle").on("click", function(e) {
        var checkAperto = $(this).parent().parent().attr("class").split(/\s+/);

        // check apertura e chiusura univoca voci primo livello
        if ($(this).parent().hasClass("dropdown-linkcont")) {
            $("#navbarMenuMobile .show").each(function() {
                $(this).removeClass("show");
            });
            $("#navbarMenuMobile .open").each(function() {
                $(this).removeClass("open");
            });
        }

        //  check per last-child: aggiunge classe per border-bottom
        if ($(this).parent().parent().is(":last-child")) {
            $("#navbarMenuMobile li.dropdown").each(function() {
                $(this).removeClass("last");
            });
            $(this).parent().parent().addClass("last");
        } else {
            $("#navbarMenuMobile li.dropdown").each(function() {
                $(this).removeClass("last");
            });
        }

        // check per border-bottom quando sottomenu livello 2 aperto
        if ($(this).parent().hasClass("dropdown-linkcont")) {
            $("#navbarMenuMobile li.dropdown").each(function() {
                $(this).removeClass("open").removeClass("aperto");
            });
        } else {
            $("#navbarMenuMobile > ul > li > ul > li.dropdown").each(function() {
                $(this).removeClass("open").removeClass("aperto");
            });
        }
        $(this).parent().parent().removeClass("open");

        // chiude sottomenù e lascia visibile solo quello selezionato
        if (!$(this).parent().next().hasClass("show")) {
            $(this).parents(".dropdown-menu").first().find(".show").removeClass("show");
        }
        if (!$(this).parent().next().hasClass("aperto")) {
            $(this).parents(".dropdown-menu").first().find(".aperto").removeClass("aperto");
        }

        var $subMenu = $(this).parent().next(".dropdown-menu");
        var $contLiMenu = $(this).parent();
        var $toggler = $(this);
        var $liCont = $(this).parent().parent();
        $subMenu.toggleClass("show");
        $contLiMenu.toggleClass("show");
        $liCont.removeClass("open").toggleClass("aperto");

        // check click se menu già aperto -> nascondo
        $.each(checkAperto, function(index, item) {
            if (item === 'aperto') {
                $liCont.removeClass("aperto");
                $subMenu.removeClass("show");
                $contLiMenu.removeClass("show");
            }
        });
        return false;
    });


    /* hover bg su primo piano home */
    if (!hasTouch()) {
        $(".boxinfo a.contentbox").hover(
            function() {
                $( this ).closest(".boxinfo").addClass("bgblue");
                $( this ).closest(".row.bgwhite").addClass("bgblue");
            }, function() {
                $( this ).closest(".boxinfo").removeClass("bgblue");
                $( this ).closest(".row.bgwhite").removeClass("bgblue");
            }
        );
    }


    /* trigger hover su img in primo piano homepage */
    $(".primo-piano .imgfull").hover(
        function(e) {
            //$(".primo-piano .boxinfo a").trigger(e.type);
            $(this).parent().parent().find(".boxinfo a").trigger(e.type);
        }
    );

    /* status hover cambio bg dots per slider news */
    // $(".boxslider a.contentbox").hover(
    //     function() {
    //         $( this ).closest('.news-slider').find('.slick-active').addClass('dotwhite');
    //     }, function() {
    //         $( this ).closest('.news-slider').find('.slick-active').removeClass('dotwhite');
    //     }
    // );
    // $(".boxslider a.contentbox").on('click', function() {
    //     var $dots = $( this ).closest('.news-slider').find('.slick-active');
    //     if (!$dots.hasClass('.dotwhite')) {
    //         $dots.addClass('dotwhite');
    //     } else {
    //         $dots.removeClass('dotwhite');
    //     }
    // });


    /* bootstrap $ tabs to accordion */
    var fakewaffle = ( function ( $, fakewaffle ) {
        'use strict';
        fakewaffle.responsiveTabs = function ( collapseDisplayed ) {
            fakewaffle.currentPosition = 'tabs';
            var tabGroups = $( '.nav-tabs.responsive' );
            var hidden    = '';
            var visible   = '';
            var activeTab = '';
            var labelTab  = tabGroups.find( 'li.first-navitem' );
            hidden = ' d-none d-lg920-flex';
            visible = ' d-lg920-none';

            if (labelTab.length) {
                var htmlFirst = $( '<div></div>', {
                    'class' : 'card-first-element' + visible,
                } ).html(labelTab.html());
                tabGroups.before().before( htmlFirst );
            }

            $.each( tabGroups, function ( index ) {
                var collapseDiv;
                var $tabGroup = $( this );
                var tabs      = $tabGroup.find( 'li a' );
                // console.log(tabs);

                if ( $tabGroup.attr( 'id' ) === undefined ) {
                    $tabGroup.attr( 'id', 'tabs-' + index );
                }

                collapseDiv = $( '<div></div>', {
                    'class' : 'card-soup responsive' + visible,
                    'id'    : 'collapse-' + $tabGroup.attr( 'id' )
                } );

                $.each( tabs, function () {
                    var $this          = $( this );
                    var oldLinkClass   = $this.attr( 'class' ) === undefined ? '' : $this.attr( 'class' );
                    var newLinkClass   = 'accordion-toggle collapsed';
                    var oldParentClass = $this.parent().attr( 'class' ) === undefined ? '' : $this.parent().attr( 'class' );
                    var newParentClass = 'card';
                    var newHash        = $this.get( 0 ).hash.replace( '#', 'collapse-' );
                    if ( oldLinkClass.length > 0 ) {
                        newLinkClass += ' ' + oldLinkClass;
                    }
                    if ( oldParentClass.length > 0 ) {
                        oldParentClass = oldParentClass.replace( /\bactive\b/g, '' );
                        newParentClass += ' ' + oldParentClass;
                        newParentClass = newParentClass.replace( /\s{2,}/g, ' ' );
                        newParentClass = newParentClass.replace( /^\s+|\s+$/g, '' );
                    }
                    if ( $this.parent().hasClass( 'active' ) ) {
                        activeTab = '#' + newHash;
                    }
                    collapseDiv.append(
                        $( '<div>' ).attr( 'class', newParentClass ).html(
                            $( '<div>' ).attr( 'class', 'card-header' ).html(
                                $( '<h4>' ).attr( 'class', 'card-title' ).html(
                                    $( '<a>', {
                                        'class'       : newLinkClass,
                                        'data-toggle' : 'collapse',
                                        'data-parent' : '#collapse-' + $tabGroup.attr( 'id' ),
                                        'href'        : '#' + newHash,
                                        'html'        : $this.html()
                                    } )
                                )
                            )
                        ).append(
                            $( '<div>', {
                                'id'    : newHash,
                                'class' : 'collapse'
                            } )
                        )
                    );
                } );

                $tabGroup.next().after( collapseDiv );
                $tabGroup.addClass( hidden );
                $( '.tab-content.responsive' ).addClass( hidden );
                if ( activeTab ) {
                    $( activeTab ).collapse( 'show' );
                }
            } );
            fakewaffle.checkResize();
            fakewaffle.bindTabToCollapse();
        };

        fakewaffle.checkResize = function () {
            if ( $( '.card-soup.responsive' ).is( ':visible' ) === true && fakewaffle.currentPosition === 'tabs' ) {
                fakewaffle.tabToPanel();
                fakewaffle.currentPosition = 'panel';
            } else if ( $( '.card-soup.responsive' ).is( ':visible' ) === false && fakewaffle.currentPosition === 'panel' ) {
                fakewaffle.panelToTab();
                fakewaffle.currentPosition = 'tabs';
            }
        };

        fakewaffle.tabToPanel = function () {
            var tabGroups = $( '.nav-tabs.responsive' );
            $.each( tabGroups, function ( index, tabGroup ) {
                // Find the tab
                var tabContents = $( tabGroup ).next( '.tab-content' ).find( '.tab-pane' );
                $.each( tabContents, function ( index, tabContent ) {
                    // Find the id to move the element to
                    var destinationId = $( tabContent ).attr( 'id' ).replace ( /^/, '#collapse-' );
                    // Convert tab to panel and move to destination
                    $( tabContent )
                        .removeClass( 'tab-pane' )
                        .addClass( 'card-body fw-previous-tab-pane' )
                        .appendTo( $( destinationId ) );
                } );
            } );
        };

        fakewaffle.panelToTab = function () {
            var panelGroups = $( '.card-soup.responsive' );
            $.each( panelGroups, function ( index, panelGroup ) {
                var destinationId = $( panelGroup ).attr( 'id' ).replace( 'collapse-', '#' );
                var destination   = $( destinationId ).next( '.tab-content' )[ 0 ];
                // Find the panel contents
                var panelContents = $( panelGroup ).find( '.card-body.fw-previous-tab-pane' );
                // Convert to tab and move to destination
                panelContents
                    .removeClass( 'card-body fw-previous-tab-pane' )
                    .addClass( 'tab-pane' )
                    .appendTo( $( destination ) );
            } );
        };

        fakewaffle.bindTabToCollapse = function () {
            var tabs     = $( '.nav-tabs.responsive' ).find( 'li a' );
            var collapse = $( '.card-soup.responsive' ).find( '.collapse' );
            // Toggle the panels when the associated tab is toggled
            tabs.on( 'shown.bs.tab', function ( e ) {
                if (fakewaffle.currentPosition === 'tabs') {
                    var $current  = $( e.currentTarget.hash.replace( /#/, '#collapse-' ) );
                    $current.collapse( 'show' );
                    if ( e.relatedTarget ) {
                        var $previous = $( e.relatedTarget.hash.replace( /#/, '#collapse-' ) );
                        $previous.collapse( 'hide' );
                    }
                }
            } );

            collapse.on('show.bs.collapse', function () {
                $('.collapse.show').each(function(){
                    $(this).collapse('hide');
                });
            });

            // Toggle the tab when the associated panel is toggled
            collapse.on( 'shown.bs.collapse', function ( e ) {
                if (fakewaffle.currentPosition === 'panel') {
                    // Activate current tabs
                    var current = $( e.target ).context.id.replace( /collapse-/g, '#' );
                    $( 'a[href="' + current + '"]' ).tab( 'show' );
                    // Update the content with active
                    var panelGroup = $( e.currentTarget ).closest( '.card-soup.responsive' );
                    $( panelGroup ).find( '.card-body' ).removeClass( 'active' );
                    $( e.currentTarget ).find( '.card-body' ).addClass( 'active' );
                }
            } );
        };

        $( window ).resize( function () {
            fakewaffle.checkResize();
        } );

        return fakewaffle;
    }( window.$, fakewaffle || { } ) );
    fakewaffle.responsiveTabs();
    /* end bootstrap $ tabs to accordion */


    /* aggiunge classe active agli accordion normali per sfondo diverso */
    $(".card-soup.accordion-widget").on("show.bs.collapse", function(e){
        var current = $(e.target);
        //current.find('.row').addClass('bgwhite');
        if ($(this).closest('.card-soup').hasClass('docentiaccordion')) {
            $(".card-soup.accordion-widget").find('.collapse.show').collapse('hide');
            current.find('.row').addClass('bgwhite');
            current.parent().find('.card-header a').addClass('bgwhite');
        } else {
            $(".card-soup.accordion-widget").find('.collapse.show').collapse('hide');
            current.addClass('bgwhite');
            current.parent().find('.card-header a').addClass('bgwhite');
        }
    });
    $(".card-soup.accordion-widget").on("hide.bs.collapse", function(e){
        var current = $(e.target);
        if ($(this).closest('.card-soup').hasClass('docentiaccordion')) {
            current.find('.row').removeClass('bgwhite');
            current.parent().find('.card-header a').removeClass('bgwhite');
        } else {
            current.removeClass('bgwhite');
            current.parent().find('.card-header a').removeClass('bgwhite');
        }
        current.removeClass('bgwhite');
        current.parent().find('.card-header a').removeClass('bgwhite');
        // $(".card-soup .card-content").each(function() {
        //     $(this).removeClass('bgwhite');
        // });
        // $(".card-soup .card-header a").each(function() {
        //     $(this).removeClass('bgwhite');
        // });
    });
    $('.card-soup .card-title').bind('click',function(){
        var self = this;
        setTimeout(function() {
            theOffset = $(self).offset();
            $('body,html').animate({ scrollTop: theOffset.top - 130 });
        }, 310); // ensure the collapse animation is done
    });


    /* menu scroll orizzontale corsi singola */
    /*var scrollDuration = 300; // duration of scroll animation
    var leftPaddle = document.getElementsByClassName('left-paddle');
    var rightPaddle = document.getElementsByClassName('right-paddle');
    // get items dimensions
    var itemsLength = $('.menu-scroll .item').length;
    var itemSize = $('.menu-scroll .item').outerWidth(true);
    var paddleMargin = 35; // get some relevant size for the paddle triggering point
    // get wrapper width
    var getMenuWrapperSize = function() {
        return $('.menu-scroll-wrapper').outerWidth();
    }
    var menuWrapperSize = getMenuWrapperSize();
    // the wrapper is responsive
    $(window).on('resize', function() {
        menuWrapperSize = getMenuWrapperSize();
        menuInvisibleSize = menuSize - menuWrapperSize;
        // get how much have we scrolled so far
        var menuPosition = getMenuPosition();
        var menuEndOffset = menuInvisibleSize - paddleMargin;
        // console.log(menuPosition +"-"+ menuEndOffset);
        // show & hide the paddles
        // depending on scroll position
        // if (menuPosition <= paddleMargin) {
        //     $(leftPaddle).addClass('d-none');
        //     $(rightPaddle).removeClass('d-none');
        // } else if (menuPosition < menuEndOffset) {
        //     // show both paddles in the middle
        //     $(leftPaddle).removeClass('d-none');
        //     $(rightPaddle).removeClass('d-none');
        // } else if (menuPosition >= menuEndOffset) {
        //     $(leftPaddle).removeClass('d-none');
        //     $(rightPaddle).addClass('d-none');
        // }
        if (menuWrapperSize < menuSize) {
            $(leftPaddle).addClass('d-none');
            $(rightPaddle).removeClass('d-none');
        } else {
            $(leftPaddle).addClass('d-none');
            $(rightPaddle).addClass('d-none');
        }
        // print important values
        $('#print-wrapper-size span').text(menuWrapperSize);
        $('#print-menu-size span').text(menuSize);
        $('#print-menu-invisible-size span').text(menuInvisibleSize);
        $('#print-menu-position span').text(menuPosition);
    });
    // size of the visible part of the menu is equal as the wrapper size
    var menuVisibleSize = menuWrapperSize;
    // get total width of all menu items
    var getMenuSize = function() {
        var sommaWidth = 0;
        $('.menu-scroll .item').each(function() {
            sommaWidth = sommaWidth + $(this).outerWidth(true);
        });
        // return itemsLength * itemSize;
        return sommaWidth;
    };
    var menuSize = getMenuSize();
    // get how much of menu is invisible
    var menuInvisibleSize = menuSize - menuWrapperSize;
    // get how much have we scrolled to the left
    var getMenuPosition = function() {
        return $('.menu-scroll').scrollLeft();
    };

    // finally, what happens when we are actually scrolling the menu
    $('.menu-scroll').on('scroll', function() {
        // get how much of menu is invisible
        menuInvisibleSize = menuSize - menuWrapperSize;
        // get how much have we scrolled so far
        var menuPosition = getMenuPosition();
        var menuEndOffset = menuInvisibleSize - paddleMargin;
        // show & hide the paddles
        // depending on scroll position
        if (menuPosition <= paddleMargin) {
            $(leftPaddle).addClass('d-none');
            $(rightPaddle).removeClass('d-none');
        } else if (menuPosition < menuEndOffset) {
            // show both paddles in the middle
            $(leftPaddle).removeClass('d-none');
            $(rightPaddle).removeClass('d-none');
        } else if (menuPosition >= menuEndOffset) {
            $(leftPaddle).removeClass('d-none');
            $(rightPaddle).addClass('d-none');
        }
    });

    // get how much have we scrolled so far
    var menuPosition = getMenuPosition();
    var menuEndOffset = menuInvisibleSize - paddleMargin;
    if (menuWrapperSize < menuSize) {
        $(leftPaddle).addClass('d-none');
        $(rightPaddle).removeClass('d-none');
    }

    // scroll to left
    $(rightPaddle).on('click', function() {
        var pageWidth = $(".menu-scroll").width();
        var questo, lefte = 0;
        $('.menu-scroll li').each(function() {
            var elementWidth = $(this).outerWidth();
            var elementLeft = $(this).position().left;
            // console.log(elementWidth +"-"+ elementLeft);
            questo = $(this);
            lefte += elementWidth;
            // console.log(lefte);
            if (pageWidth - (elementWidth + elementLeft) < 0) {
                // console.log("overflow!");
                lefte -= elementWidth - 25;
                return false;
            } else {
                // console.log("doesn't overflow");
            }
        });
        // $('.menu-scroll').animate( { scrollLeft: menuInvisibleSize}, scrollDuration);
        $('.menu-scroll').animate( { scrollLeft: lefte }, scrollDuration);
    });
    // scroll to right
    $(leftPaddle).on('click', function() {
        $('.menu-scroll').animate( { scrollLeft: '0' }, scrollDuration);
    });

    $('#print-wrapper-size span').text(menuWrapperSize);
    $('#print-menu-size span').text(menuSize);
    $('#print-menu-invisible-size span').text(menuInvisibleSize);*/


    /* bg snodo larghezza */
    bgSnodoBande();
    $( window ).bind( "resize", bgSnodoBande );
    function bgSnodoBande() {
        if ($(".bgalt-image")[0]) {
            var marginleftsn = $(".bgleft").offset().left;
            var marginrightsn = ($(window).width() - ($(".bgright").offset().left + $(".bgright").outerWidth()));
            var stylebg = '<style id="myStyle">';
            stylebg += '.bgalt-image .content-snodo .bgleft:after { left: -'+marginleftsn+'px; width: calc(100% + '+marginleftsn+'px); }';
            stylebg += '.bgalt-image .content-snodo .bgright:after { right: -'+marginrightsn+'px; width: calc(100% + '+marginrightsn+'px); }';
            stylebg += '</style>';
            $('#myStyle').remove();
            $( stylebg ).appendTo( "head" );
        }
    }


    /* browser detect */
    var data = [
        {str:navigator.userAgent,sub:'Chrome',ver:'Chrome',name:'chrome'},
        {str:navigator.vendor,sub:'Apple',ver:'Version',name:'safari'},
        {prop:window.opera,ver:'Opera',name:'opera'},
        {str:navigator.userAgent,sub:'Firefox',ver:'Firefox',name:'firefox'},
        {str:navigator.userAgent,sub:'MSIE',ver:'MSIE',name:'ie'},
        {str:navigator.userAgent,sub:'Trident',ver:'Trident',name:'ie'}];
    for (var n=0;n<data.length;n++) {
        if ((data[n].str && (data[n].str.indexOf(data[n].sub) != -1)) || data[n].prop) {
            var v = function(s){var i=s.indexOf(data[n].ver);return (i!=-1)?parseInt(s.substring(i+data[n].ver.length+1)):'';};
            $('html').addClass(data[n].name+' '+data[n].name+v(navigator.userAgent) || v(navigator.appVersion)); break;
        }
    }


    /* fix IE per object-fit cover */
    var userAgent, ieReg, ie;
    userAgent = window.navigator.userAgent;
    ieReg = /msie|Trident.*rv[ :]*11\./gi;
    ie = ieReg.test(userAgent);
    if(ie) {
        $(".home-slider .slick-slide").each(function () {
            var $container = $(this),
                imgUrl = $container.find("img").prop("src");
            if (imgUrl) {
                $container.css("backgroundImage", 'url(' + imgUrl + ')').addClass("custom-object-fit");
            }
        });
    }


    /* ul to select per menu corsi */
    function ulToSelect() {
        var myElem = document.getElementById('menu-scroll-select');
        if (myElem === null) {
            $('ul.menu-scroll').each(function(){
                var list = $(this),
                select = $(document.createElement('select')).attr("id","menu-scroll-select").insertBefore($(this).hide()).change(function(){
                    window.open($(this).val(),'_self')
                });

                $('>li a', this).each(function(){
                    var option = $(document.createElement('option'))
                    .appendTo(select)
                    .val(this.href)
                    .html($(this).html());
                    if($(this).attr('class') === 'selected'){
                        option.attr('selected','selected');
                    }
                });

                list.hide();
            });
        }
    }
    $.fn.outerHTML = function() {
        return $('<div />').append(this.eq(0).clone()).html();
    };
    function ulToDropdown() {
        if($(".menu-scroll-wrapper .dropdown").length == 0) {
            var heightHead = $("header.head").outerHeight();
            var original = $('ul.menu-scroll').outerHTML();
            var liactive = $('ul.menu-scroll li.active').first().find('a').text();
            var htmlbefore = '<div class="dropdown"><button class="drop-corsomenu dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">'+liactive+'</button>';
            var htmlafter = '</div>';
            $('ul.menu-scroll').first().addClass('dropdown-menu').attr('role', 'menu');
            $('.menu-scroll-container').addClass('nopadding').css('top', heightHead + "px");
            $('ul.menu-scroll.dropdown-menu li').each(function() {
                $(this).addClass('dropdown-item');
            });
            var htmlul = $('ul.menu-scroll').first().outerHTML();
            $(".menu-scroll-wrapper").html(htmlbefore+htmlul+htmlafter);
            $('<div class="originalmenu">'+original+'</div>').insertAfter( ".menu-scroll-wrapper .dropdown" ).hide();
        } else {
            var heightHead = $("header.head").outerHeight();
            $(".menu-scroll-wrapper .dropdown").show();
            $('.menu-scroll-container').css('top', heightHead + "px");
        }
    }
    if (window.outerWidth < 992) {
        var heightHead = $("header.head").outerHeight();
        ulToDropdown();
        $(".originalmenu").hide();
        $('.menu-scroll-container').addClass('nopadding');
        $(".menu-scroll-wrapper .dropdown").show();
    } else {
        var heightHead = $("header.head").outerHeight();
        $(".originalmenu").show();
        $(".menu-scroll-wrapper .dropdown").hide();
        $('.menu-scroll-container').removeClass('nopadding').removeAttr('style');
        // $('ul.menu-scroll').show();
    }
    $(window).on("resize", function(){
        var heightHead = $("header.head").outerHeight();
        if (window.outerWidth <= 992) {
            ulToDropdown();
            $(".originalmenu").hide();
            $('.menu-scroll-container').addClass('nopadding');
            $(".menu-scroll-wrapper .dropdown").show();
        } else {
            $(".originalmenu").show();
            $(".menu-scroll-wrapper .dropdown").hide();
            $('.menu-scroll-container').removeClass('nopadding').removeAttr('style');
            // $('ul.menu-scroll').show();
        }
    });


    /* select styling custom */
    $("select.chosen-select").chosen({
        disable_search: true,
        disable_search_threshold: 100,
        inherit_select_classes: true,
        width: "100%",
        scroll_to_highlighted: false
    }).on('chosen:showing_dropdown', function (evt, params) {
        $(this).parent().addClass('is-active');
        $(this).parent().find('.chosen-results li.hidden').hide();
    }).on('chosen:hiding_dropdown', function (evt, params) {
        $(this).parent().removeClass('is-active');
        $(this).parent().find('.chosen-results li.hidden').show();
    });
    $('.chosen-results').off( 'DOMMouseScroll' );


    /* lista lettere invio ricerca form */
    $('.ricerca-lettere-list li a').on('click', function() {
        var lettera = $(this).data('letter');
        $("#ricerca-lettere input").val(lettera);
        $("#ricerca-lettere").submit();
    });


    /* reset form ricerca */
    $('#cancella-filtri').on('click', function(e) {
        e.preventDefault();
        $(".chosen-select").val('').trigger("chosen:updated");
        $('#ricerca-lettere').trigger("reset");
        $('#ricerca-dipartimento').trigger("reset");
        $('#ricerca-facolta').trigger("reset");
        $('#ricerca-ruolo').trigger("reset");
        $('#ricerca-nomecognome').trigger("reset");
        $('#ricerca-libera').trigger("reset");
        $('#ricerca-data').trigger("reset");
        $('#ricerca-newscat .mdb-select').val('').change();
        if ($('#ricerca-newscat').length > 0) {
            $('#ricerca-newscat .multiple-select-dropdown').append('<button class="btn-save btn btn-primary btn-sm">Salva</button>');
        }
    });


    /* trigger invio form onchange opzioni select */
    $('.chosen-select').on('change', function(evt, params) {
        var form = $(this).closest("form");
        form.trigger("submit");
    });


    /* auto selected lettera se parametro presente in url */
    function getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };
    if ($('.ricerca-lettere-list').length > 0) {
        var letteraScelta = getUrlParameter('ricerca-lettera');
        $('.ricerca-lettere-list li a[data-letter="' + letteraScelta + '"').addClass('selected');
    }


    /* altezza slider master home */
    // if ($(".navmenu-slider").length > 0) {
    //     var altezzaNav = $(".navmenu-slider").outerHeight();
    // }


    /* hover su voci menu master home */
    function toggleDropdownMaster (e) {
        var _d = $(e.target).closest('.dropdown'),
          _m = $('.dropdown-menu', _d);
        if (window.outerWidth >= 992) {
            setTimeout(function(){
            var shouldOpen = e.type !== 'click' && _d.is(':hover');
            _m.toggleClass('show', shouldOpen);
            _d.toggleClass('show', shouldOpen);
            $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
            }, e.type === 'mouseleave' ? 0 : 0);
        }
    }
    function checkResizeToggleMenu() {
        if (window.outerWidth >= 992) {
            $('.navmenu-slider').removeAttr('style');
            $('body').on('mouseenter mouseleave','.slider-menunav .dropdown',toggleDropdownMaster).on('click', '.slider-menunav .dropdown-menu a', toggleDropdownMaster);
        } else {
            // var heightHead = $("header.head").outerHeight();
            // $('.navmenu-slider').css('top', heightHead + "px");
        }
    }
    checkResizeToggleMenu();
    $(window).on("resize", function(){
        checkResizeToggleMenu();
    });


    /*  voce menu attiva in evidenza master home */
    // var voceAttiva = $(".slider-menunav .dropdown.active a").first().text();
    // $("#btn-shownavcorso span").text(voceAttiva);


    /* voci menu interlinea */
    $("#mainmenuContent.tab-content .col").each(function() {
        $(this).find('h4:eq(1)').css('min-height', 'inherit');
        $(this).find('h4:eq(1)').addClass('mt-30');
    });


    /* fix menu master on scroll */
    if ($('.master-home').length > 0) {
        if ($('.master-home .navmenu-slider').length > 0) {
            window.onscroll = function() { fixMasterMenu() };
            var navbar = document.getElementsByClassName("navmenu-slider")[0];
            var sticky = navbar.offsetTop;
            function fixMasterMenu() {
                if (window.pageYOffset >= sticky) {
                    navbar.classList.add("sticky")
                } else {
                    navbar.classList.remove("sticky");
                }
            }
        }
    }

    /* nascondi quando ci sono più di 3 eventi */
    if ($('.oggi-ateneo').length > 0) {
        var $ShowHideMore = $('.container-eventiateneo');
        var $times = $(".oggi-ateneo .rigaevento");
        if ($times.length > 3) {
            $ShowHideMore.children(':nth-of-type(n+4)').addClass('moreShown');
        } else {
            $("#vedituttieventi").hide();
        }
    }
    $("#vedituttieventi").on("click", function(e) {
        e.preventDefault();
        $("#vedituttieventi").toggleClass('vedi');
        if ($(this).hasClass('vedi')) {
            $(".oggi-ateneo .rigaevento").each(function() {
                $(this).removeClass('moreShown');
            });
        } else {
            var $ShowHideMore = $('.container-eventiateneo');
            var $times = $(".oggi-ateneo .rigaevento");
            if ($times.length > 3) {
                $ShowHideMore.children(':nth-of-type(n+4)').addClass('moreShown');
            }
        }
    });

    /* select mdb per magazine */
    if ($('main.magazinenews').length > 0) {
        $('.mdb-select').materialSelect();
    }

    /* audio player */
    if ($('.extra .sezione-magazine').length > 0) {
        $("#radioiulm-player").jPlayer({
            ready: function () {
            $(this).jPlayer("setMedia", {
                mp3: "https://dreamsiteradiocp3.com/proxy/radioiulm?mp=/stream"
            });
            },
            size: {
                width: "100%",
                height: "auto"
            },
            swfPath: "../../images",
            supplied: "mp3"
        });

        $(".radioiulm-container .jp-play").on("click", function() {
           $(this).hide();
           $(".radioiulm-container .jp-pause").show();
        });
        $(".radioiulm-container .jp-pause").on("click", function() {
            $(this).hide();
            $(".radioiulm-container .jp-play").show();
        });
    }

    /* datepicker news */
    $('.datepicker').pickadate({
        weekdaysShort: ['D', 'L', 'M', 'M', 'G', 'V', 'S'],
    });

    /* search header open menu */
    $(".link-visibili .search-menu").on("click", function(e) {
        e.preventDefault();
        $("a.search").trigger( "click" );
        if (!$("header.head").hasClass("active")) {
            $(".nav-desktop .navbar-toggler").trigger( "click" );
        }
        $(".search-input").select().focus();
        // $(".link-visibili .search-menu").hide();
    });

    /* new widget contatti */
    $('.box-new-contatti ul.nav a').on('click', function(e) {
        e.preventDefault();
        //$(this).tab('show');
    });
	
	$(".search_icon").click(function(){
		var _this = $(this);
		_this.closest("body").toggleClass("search-input-on");
		_this.prev().focus();
	});

});
